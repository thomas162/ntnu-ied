# NTNU-IED

All code is adapted from thhe libIEC61850 project located here: 
https://github.com/mz-automation/libiec61850

There are 3 programs adapted and 2 simulink models utilized, in addition to 
custom scripts for changing system variables and Simulink values:

NTNU-IED Folder: Contains IED programs that to connect to, and 
receive data from Simulink as well as store the data from Simulink in datasets
on the IED for retrival from the HMI. This is customized for each IED

NTNU-HMI Folder: Contains 2 programs which is customized for conenction to the IEDs: 
1. A IED Client program used to simulate a HMI, 
displaying data from a given IED
2. A IED Client program used to send commands to an IED

NTNU-EW Folder: Contains 2 programs:
1. The basic TCP/IP program used to display data from the HMI
2. A basic TCP/IP program used to control the functions of an IED through 
the HMI.

Simulink folder: Contains 2 Simulink models that the IED operate on/with