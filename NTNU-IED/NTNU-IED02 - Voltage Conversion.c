/*
 *  Derived from libIEC61850 server_example_basic_io.c example
 *
 *  - How to use simple control models
 *  - How to serve analog measurement data
 *  - Using the IedServerConfig object to configure stack features
 */

#include "iec61850_server.h"
#include "hal_thread.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "static_model.h"

/*
 * Added to facilitate communication with Matlab/Simulink over TCP/IP
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h> // for open
#include <unistd.h> // for close
#include <string.h> 

/* import IEC 61850 device model created from SCL-File */
extern IedModel iedModel;

static int running = 0;
static IedServer iedServer = NULL;
static char debug[5] = "true";


/*
 * The following is a customization of the example to facilitate communication with Matlab/Simulink over TCP/IP
 * Calls to some of these has also been added in the main() function and processing of incoming messages
 * START CUSTOMIZATION
 */

 
#define MAX_BUF 1024 
#define PORT 8165 // default 8165 - Used for interaction with Simulink
#define PORTHMI 8166 // default 8166 - Used for interaction with HMI
#define IEDPORT 102 //default 102

#define SA struct sockaddr 


#define HISTORIAN_IP_ADDRESS "10.40.40.1"
#define PORTINFLUX 8086 // default 8086 - Used for sending metric to InfluxDB
#define HOSTNAME "NTNU-IED01"
#define DATABASE "influxdb"                /* This is the InfluxDB database name */
#define USERNAME "influx"                 /* These are the credentials used to access the database */
#define PASSWORD "influxdb"

#define SA struct sockaddr 

 pexit(char * msg)
{
        perror(msg);
        exit(1);
}

// Function to forward metrics to InfluxDB for Historian, adapted from https://www.ibm.com/support/pages/uploading-stats-influxdb-c-language
void sendToInflux(char* sensor, float value)
{
	int i;
    int sockfd;
    int loop;
    int ret;
    char header[BUFSIZE];
    char body[BUFSIZE];
    char result[BUFSIZE];

	static struct sockaddr_in serv_addr; /* static is zero filled on start up */

	printf("Connecting socket to %s and port %d\n", HISTORIAN_IP_ADDRESS, PORTINFLUX);
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    	pexit("socket() failed");
		 serv_addr.sin_family = AF_INET;
         serv_addr.sin_addr.s_addr = inet_addr(HISTORIAN_IP_ADDRESS);
         serv_addr.sin_port = htons(PORTINFLUX);
         if (connect(sockfd, (struct sockaddr*) & serv_addr, sizeof(serv_addr)) < 0)
              pexit("connect() failed");
    
        	for (loop = 0; i < LOOPS; i++) {
            /* InfluxDB line protocol note:
               measurement name is "noise"
               tag is host=blue - multiple tags separate with comma
               data is random=<number>
               ending epoch time missing (3 spaces) so InfluxDB generates the timestamp */
               /* InfluxDB line protocol note: ending epoch time missing so InfluxDB greates it */
               sprintf(body, "%s,host=%s float=%.4f   \n", sensor, HOSTNAME, value);
        
               /* Note spaces are important and the carriage-returns & newlines */
               /* db= is the datbase name, u= the username and p= the password */
               sprintf(header,
                       "POST /write?db=%s&u=%s&p=%s HTTP/1.1\r\nHost: influx:8086\r\nContent-Length: %ld\r\n\r\n",
                        DATABASE, USERNAME, PASSWORD, strlen(body));
    
               printf("Send to InfluxDB the POST request bytes=%d \n->|%s|<-\n", strlen(header), header);
                  write(sockfd, header, strlen(header));
                  if (ret < 0)
                          pexit("Write Header request to InfluxDB failed");
        
                      printf("Send to InfluxDB the data bytes=%d \n->|%s|<-\n", strlen(body), body);
                  ret = write(sockfd, body, strlen(body));
                  if (ret < 0)
                         pexit("Write Data Body to InfluxDB failed");
        
                      /* Get back the acknwledgement from InfluxDB */
                      /* It worked if you get "HTTP/1.1 204 No Content" and some other fluff */
                      ret = read(sockfd, result, sizeof(result));
                  if (ret < 0)
                          pexit("Reading the result from InfluxDB failed");
                  result[ret] = 0; /* terminate string */
                  printf("Result returned from InfluxDB. Note:204 is Sucess\n->|%s|<-\n", result);
        
                      printf(" - - - sleeping for %d secs\n", SECONDS);
                  sleep(SECONDS);
        
    }
          close(sockfd);
}

// Function designed for chat between client and server - Simulink. 
void simulink_data(int sockfd) 
{ 
	char buff[MAX_BUF]; 
	int n; 
	// infinite loop for chat 
	for (;;) { 
		bzero(buff, MAX_BUF); 

		// read the message from client and copy it in buffer 
		read(sockfd, buff, sizeof(buff)); 
		
		// Initialize IED and communication
		uint64_t timestamp = Hal_getTimeInMs();
		Timestamp iecTimestamp;

        Timestamp_clearFlags(&iecTimestamp);
        Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
        Timestamp_setLeapSecondKnown(&iecTimestamp, true);
		
		// print buffer which contains the client contents 	
		printf("Raw data from client: %s\n", buff); 
		
		if (strncmp("Start", buff, 5) == 0)
		{
			printf("Startposition - Starting processing\n"); 
		}
		else if (strncmp("End", buff, 4) == 0)
		{
			printf("Endposition - Ending processing\n"); 
			break;
		}
		else {
			//Process data and separate it, for the 4 first sensor values, update new values on the IED
			char * pch;
			int o;
			o = 0;
			pch = strtok(buff," ");
			while (pch != NULL)
			{
				if (o == 0)
				{
				    printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("DC Conversion value: %s\n", pch);
					endToInflux("DC Conversion value", value);
				}
				else if (o == 1)
				{
				    printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("DC Conversion value 2: %s\n", pch);
					sendToInflux("DC Conversion value 2", value);
				}				
				else if (o == 2)
				{
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 1: converted");
					sendToInflux("Sensor value 1", value);
					
					MmsValue* sensor1Value = MmsValue_newFloat(value);
					MmsValue* sensorTimestamp = MmsValue_newUtcTime(time(NULL));
					
					// Lock the IED model
					IedServer_lockDataModel(iedServer);
					
					// Set correct timestamp
					MmsValue_setUtcTime(sensorTimestamp, time(NULL));
					
					// Update sensor valeu
					IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn1_mag_f, sensor1Value);
					IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn1_t, sensorTimestamp);
					
					// Release IED model
					IedServer_unlockDataModel(iedServer);

				}
				else if (o == 3)
				{
					printf("Sensor value 2: %s\n", pch);
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 2: converted");
					sendToInflux("Sensor value 2", value);
					
					IedServer_lockDataModel(iedServer);
					IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn2_t, &iecTimestamp);
					IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn2_mag_f, value);
					
					// Release IED model
					IedServer_unlockDataModel(iedServer);
				}
				else if (o == 4)
				{
					printf("Sensor value 3: %s\n", pch);
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 3: converted");
					sendToInflux("Sensor value 3", value);
					
					IedServer_lockDataModel(iedServer);
					IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn3_t, &iecTimestamp);
					IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn3_mag_f, value);
					
					// Release IED model
					IedServer_unlockDataModel(iedServer);
				}
				else if (o == 5)
				{
					printf("Sensor value 4: %s\n", pch);
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 4: converted");
					sendToInflux("Sensor value 4", value);
					
					IedServer_lockDataModel(iedServer);
					IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn4_t, &iecTimestamp);
					IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn4_mag_f, value);
					
					// Release IED model
					IedServer_unlockDataModel(iedServer);
				}
				else if (o == 6)
				{
					printf("Sensor value 5: %s\n", pch);
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 5: converted");
					sendToInflux("Sensor value 5", value);
				}
				else if (o == 7)
				{
					printf("Sensor value 6: %s\n", pch);
					printf("Float value: %4.8f\n",atof(pch));
					double orig = atof(pch);
					float value = (float)orig;
					printf("Sensor value 6: converted");
					sendToInflux("Sensor value 6", value);
				}
				o++;
				
				pch = strtok(NULL, " ");
			}
		}
		
		
		// Zero out the buffer
		bzero(buff, MAX_BUF); 
		n = 0; 
		// copy server message in the buffer 
		while ((buff[n++])) 
			; 

		// and send that buffer to client 
		write(sockfd, buff, sizeof(buff)); 

	} 
} 

// Function designed for chat between client and server - HMI. 
void hmi_data(int sockfd) 
{ 
	char buff[MAX_BUF]; 
	int n; 
	// infinite loop for chat 
	for (;;) { 
		bzero(buff, MAX_BUF); 

		// read the message from client and copy it in buffer 
		read(sockfd, buff, sizeof(buff)); 
		
		// print buffer which contains the client contents 	
		printf("Raw data from client: %s\n", buff); 
		
		//Process data and separate it, should only contain asingle value...
		char * pch;
		int o;
		o = 0;
		pch = strtok(buff," ");
		while (pch != NULL)
		{
			if (o == 0)
			{
				printf("Command value: %s\n", pch);
				// Send changed value to Simulink - Voltage Conversion
				socket_client("10.40.70.11", 36881, pch);
			}
			else if (o > 0)
			{
				printf("More than one value receieved, ignoring..);
			}	
			o++;
			pch = strtok(NULL, " ");
		}
		// Zero out the buffer
		bzero(buff, MAX_BUF); 
		n = 0; 
		// copy server message in the buffer 
		while ((buff[n++])) 
			; 

		// and send that buffer to client 
		write(sockfd, buff, sizeof(buff)); 

	} 
} 

//Function to handle connection between Client and Server - IED to Simulink
void socket_client(char srv_addr, int srv_port, char command)
{
	int sockfd, connfd; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and varification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr(srv_addr); 
    servaddr.sin_port = htons(srv_port); 
  
    // connect the client socket to server socket 
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
        printf("connection with the server failed...\n"); 
        exit(0); 
    } 
    else
        printf("connected to the server..\n"); 
  
    // function for chat 
    simulink_client(sockfd, command); 
  
    // close the socket 
    close(sockfd); 
}

//Function for chat between Client and Server - IED to Simulink
void simulink_client(int sockfd, char command)
{
	char buff[1024]; 
    for (;;) { 
        // clear the buffer
        bzero(buff, sizeof(buff)); 
        // write the command to Simulink
        write(sockfd, command, sizeof(command)); 
        // read the buffer and display output
		read(sockfd, buff, sizeof(buff)); 
        printf("Reply from Server : %s", buff); 
        if ((strncmp(buff, "exit", 4)) == 0) { 
            printf("Client Exit...\n"); 
            break; 
        } 
    } 
}

// Server socket function - HMI
int socket_server_hmi() 
{ 
	int sockfd, connfd, len, tmpid; 
	struct sockaddr_in servaddr, cli; 

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("Socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created..\n"); 
		
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORTHMI); 

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("Socket bind failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully binded..\n"); 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 5)) != 0) { 
		printf("Listen failed...\n"); 
		exit(0); 
	} 
	else
		printf("Server listening..\n"); 
		printf("Using port number %d\n",PORT); 
		printf("Buffer size set to %d\n", MAX_BUF); 
	len = sizeof(cli); 
	
	// Make sure the socket stays open until manually closed
	
	while (1) 
	{
		// Accept the data packet from client and verification 
		connfd = accept(sockfd, (SA*)&cli, &len); 
		if (connfd < 0) { 
			printf("Server acccept failed...\n"); 
			exit(0); 
		} 
		else
			printf("Server acccept the client...\n"); 
			// Fork the process to allow multiple instances
			tmpid = fork();
			if(tmpid < 0)
			{
				perror("fork failed..\n");
			}
			if (tmpid == 0)
			{
				printf("Processing...\n"); 
				// Discard the initial socket
				close(sockfd); 
				// Call the function for chatting between client and server 	
				hmi_data(connfd); 
				exit(0);
			}
			else {
				close(connfd);
			}
	}	
	// After chatting close the socket 
	close(sockfd); 
} 

// Server socket function - Simulink
int socket_server() 
{ 
	int sockfd, connfd, len, tmpid; 
	struct sockaddr_in servaddr, cli; 

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("Socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created..\n"); 
		
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORT); 

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("Socket bind failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully binded..\n"); 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 5)) != 0) { 
		printf("Listen failed...\n"); 
		exit(0); 
	} 
	else
		printf("Server listening..\n"); 
		printf("Using port number %d\n",PORT); 
		printf("Buffer size set to %d\n", MAX_BUF); 
	len = sizeof(cli); 
	
	// Make sure the socket stays open until manually closed
	
	while (1) 
	{
		// Accept the data packet from client and verification 
		connfd = accept(sockfd, (SA*)&cli, &len); 
		if (connfd < 0) { 
			printf("Server acccept failed...\n"); 
			exit(0); 
		} 
		else
			printf("Server acccept the client...\n"); 
			// Fork the process to allow multiple instances
			tmpid = fork();
			if(tmpid < 0)
			{
				perror("fork failed..\n");
			}
			if (tmpid == 0)
			{
				printf("Processing...\n"); 
				// Discard the initial socket
				close(sockfd); 
				// Call the function for chatting between client and server 	
				simulink_data(connfd); 
				exit(0);
			}
			else {
				close(connfd);
			}
	}	
	// After chatting close the socket 
	close(sockfd); 
} 


/*
 * END CUSTOMIZATION
 */


void
sigint_handler(int signalId)
{
    running = 0;
}

static ControlHandlerResult
controlHandlerForBinaryOutput(void* parameter, MmsValue* value, bool test)
{
    if (test)
        return CONTROL_RESULT_FAILED;

    if (MmsValue_getType(value) == MMS_BOOLEAN) {
        printf("received binary control command: ");

        if (MmsValue_getBoolean(value))
            printf("on\n");
        else
            printf("off\n");
    }
    else
        return CONTROL_RESULT_FAILED;

    uint64_t timeStamp = Hal_getTimeInMs();

    if (parameter == IEDMODEL_GenericIO_GGIO1_SPCSO1) {
        IedServer_updateUTCTimeAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO1_t, timeStamp);
        IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO1_stVal, value);
    }

    if (parameter == IEDMODEL_GenericIO_GGIO1_SPCSO2) {
        IedServer_updateUTCTimeAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO2_t, timeStamp);
        IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO2_stVal, value);
    }

    if (parameter == IEDMODEL_GenericIO_GGIO1_SPCSO3) {
        IedServer_updateUTCTimeAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO3_t, timeStamp);
        IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO3_stVal, value);
    }

    if (parameter == IEDMODEL_GenericIO_GGIO1_SPCSO4) {
        IedServer_updateUTCTimeAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO4_t, timeStamp);
        IedServer_updateAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO4_stVal, value);
    }

    return CONTROL_RESULT_OK;
}


static void
connectionHandler (IedServer self, ClientConnection connection, bool connected, void* parameter)
{
    if (connected)
        printf("Connection opened\n");
    else
        printf("Connection closed\n");
}


int
main(int argc, char** argv)
{
    printf("Using libIEC61850 version %s\n", LibIEC61850_getVersionString());
	
	
	
    /* Create new server configuration object */
    IedServerConfig config = IedServerConfig_create();

    /* Set buffer size for buffered report control blocks to 200000 bytes */
    IedServerConfig_setReportBufferSize(config, 200000);

    /* Set stack compliance to a specific edition of the standard (WARNING: data model has also to be checked for compliance) */
    IedServerConfig_setEdition(config, IEC_61850_EDITION_2);

    /* Set the base path for the MMS file services */
    IedServerConfig_setFileServiceBasePath(config, "./vmd-filestore/");

    /* disable MMS file service */
    IedServerConfig_enableFileService(config, false);

    /* enable dynamic data set service */
    IedServerConfig_enableDynamicDataSetService(config, true);

    /* disable log service */
    IedServerConfig_enableLogService(config, true);

    /* set maximum number of clients */
    IedServerConfig_setMaxMmsConnections(config, 2);

    /* Create a new IEC 61850 server instance */
    iedServer = IedServer_createWithConfig(&iedModel, NULL, config);

    /* configuration object is no longer required */
    IedServerConfig_destroy(config);

    /* Install handler for operate command */
    IedServer_setControlHandler(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO1,
            (ControlHandler) controlHandlerForBinaryOutput,
            IEDMODEL_GenericIO_GGIO1_SPCSO1);

    IedServer_setControlHandler(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO2,
            (ControlHandler) controlHandlerForBinaryOutput,
            IEDMODEL_GenericIO_GGIO1_SPCSO2);

    IedServer_setControlHandler(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO3,
            (ControlHandler) controlHandlerForBinaryOutput,
            IEDMODEL_GenericIO_GGIO1_SPCSO3);

    IedServer_setControlHandler(iedServer, IEDMODEL_GenericIO_GGIO1_SPCSO4,
            (ControlHandler) controlHandlerForBinaryOutput,
            IEDMODEL_GenericIO_GGIO1_SPCSO4);

    IedServer_setConnectionIndicationHandler(iedServer, (IedConnectionIndicationHandler) connectionHandler, NULL);

    /* MMS server will be instructed to start listening for client connections. */
    IedServer_start(iedServer, IEDPORT);

    if (!IedServer_isRunning(iedServer)) {
        printf("Starting server failed (maybe need root permissions or another server is already using the port)! Exit.\n");
        IedServer_destroy(iedServer);
        exit(-1);
    }

    running = 1;

    signal(SIGINT, sigint_handler);

    float t = 0.f;

    while (running) {
		
		
        
		uint64_t timestamp = Hal_getTimeInMs();

        t += 0.1f;

        float an1 = sinf(t);
        float an2 = sinf(t + 1.f);
        float an3 = sinf(t + 2.f);
        float an4 = sinf(t + 3.f);

        Timestamp iecTimestamp;

        Timestamp_clearFlags(&iecTimestamp);
        Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
        Timestamp_setLeapSecondKnown(&iecTimestamp, true);

         /* toggle clock-not-synchronized flag in timestamp */
         if (((int) t % 2) == 0)
            Timestamp_setClockNotSynchronized(&iecTimestamp, true);

        IedServer_lockDataModel(iedServer);

        //CUSTOMIZED
        // Here we insert values from the Simulink model to the datasets
 
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn1_t, &iecTimestamp);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn1_mag_f, an1);

        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn2_t, &iecTimestamp);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn2_mag_f, an2);

        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn3_t, &iecTimestamp);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn3_mag_f, an3);

        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn4_t, &iecTimestamp);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_GenericIO_GGIO1_AnIn4_mag_f, an4);

        IedServer_unlockDataModel(iedServer);

	    /* Create a TCP/IP socket and start waiting for communication from MATLAB */
		socket_server();

			Thread_sleep(100);
		}

    /* stop MMS server - close TCP server socket and all client sockets */
    IedServer_stop(iedServer);

    /* Cleanup - free all resources */
    IedServer_destroy(iedServer);

} /* main() */
