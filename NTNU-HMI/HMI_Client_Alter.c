/*
 * Based on client_example_control.c
 *
 * Used to receive command and send them to the Simulink model
 */
#include "iec61850_client.h"
#include "hal_thread.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h> // for open
#include <unistd.h> // for close
#include <string.h> 


#define MAX_BUF 1024 
#define PORT 8200 //TCP/IP Port used to connumicate with the EW
#define IEDPORT 1202 //default 102
#define SA struct sockaddr 


// Function to send commands to an IED, operation target currently hardcoded, but can be adapted to be more dynamic
void sendCommandToIED(int argc, char* argv, char value)
{
	char* hostname;
    int tcpPort = IEDPORT;

    if (argc > 1)
        hostname = argv[1];
    else
        hostname = "localhost";

    if (argc > 2)
        tcpPort = atoi(argv[2]);

    IedClientError error;

    IedConnection con = IedConnection_create();

    IedConnection_connect(con, &error, hostname, tcpPort);

    if (error == IED_ERROR_OK) {


        /************************
         * Direct control
         ***********************/

        ControlObjectClient control
            = ControlObjectClient_create("simpleIOGenericIO/GGIO1.SPCSO1", con);

        MmsValue* ctlVal = MmsValue_newBoolean(true);

        //ControlObjectClient_setOrigin(control, NULL, 3);
		ControlObjectClient_setOrigin(control, NULL, (int)value);

        if (ControlObjectClient_operate(control, ctlVal, 0 /* operate now */)) {
            printf("simpleIOGenericIO/GGIO1.SPCSO1 operated successfully\n");
        }
        else {
            printf("failed to operate simpleIOGenericIO/GGIO1.SPCSO1\n");
        }

        MmsValue_delete(ctlVal);

        ControlObjectClient_destroy(control);

        /* Check if status value has changed */

        MmsValue* stVal = IedConnection_readObject(con, &error, "simpleIOGenericIO/GGIO1.SPCSO1.stVal", IEC61850_FC_ST);

        if (error == IED_ERROR_OK) {
            bool state = MmsValue_getBoolean(stVal);
            MmsValue_delete(stVal);

            printf("New status of simpleIOGenericIO/GGIO1.SPCSO1.stVal: %i\n", state);
        }
        else {
            printf("Reading status for simpleIOGenericIO/GGIO1.SPCSO1 failed!\n");
        }
		
        IedConnection_close(con);
    }
    else {
    	printf("Connection failed!\n");
    }

    IedConnection_destroy(con);
}

// Function to process data coming from the EW
void hmi_processor(char buff[MAX_BUF], char iedIP, int iedPort)
{
		// Process the incomoing TCP/IP data and perform actions on a given IED
		printf("Inside EW Processor\n");
		char * pch;
		
		pch = strtok(buff," ");
		while (pch != NULL)
		{
				printf("Value received: %s\n", pch);
				// Send command to the HMI program to perform action on IED
				sendCommandToIED(iedIP, iedPort, pch);
			
			pch = strtok(NULL, " ");
		}
		
		// Zero out the buffer
		bzero(buff, MAX_BUF); 
}

// Server socket function 
int socket_server() 
{ 
	int sockfd, connfd, len, tmpid; 
	struct sockaddr_in servaddr, cli; 

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("Socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created..\n"); 
		
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORT); 

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("Socket bind failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully binded..\n"); 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 5)) != 0) { 
		printf("Listen failed...\n"); 
		exit(0); 
	} 
	else
		printf("Server listening..\n"); 
		printf("Using port number %d\n",PORT); 
		printf("Buffer size set to %d\n", MAX_BUF); 
	len = sizeof(cli); 
	
	// Make sure the socket stays open until manually closed
	
	while (1) 
	{
		// Accept the data packet from client and verification 
		connfd = accept(sockfd, (SA*)&cli, &len); 
		if (connfd < 0) { 
			printf("Server acccept failed...\n"); 
			exit(0); 
		} 
		else
			printf("Server acccept the client...\n"); 
			// Fork the process to allow multiple instances
			tmpid = fork();
			if(tmpid < 0)
			{
				perror("fork failed..\n");
			}
			if (tmpid == 0)
			{
				printf("Processing...\n"); 
				// Discard the initial socket
				
				
				
				// close(sockfd); 
				// Call the function for chatting between client and server 	
				// ew_processor(connfd); 
				//exit(0);
			}
			else {
				close(connfd);
			}
	}	
	// After chatting close the socket 
	close(sockfd); 
} 



static void commandTerminationHandler(void *parameter, ControlObjectClient connection)
{


    LastApplError lastApplError = ControlObjectClient_getLastApplError(connection);

    /* if lastApplError.error != 0 this indicates a CommandTermination- */
    if (lastApplError.error != 0) {
        printf("Received CommandTermination-.\n");
        printf(" LastApplError: %i\n", lastApplError.error);
        printf("      addCause: %i\n", lastApplError.addCause);
    }
    else
        printf("Received CommandTermination+.\n");
}


int main(int argc, char** argv) {
	socket_server();
    
}


