#include <stdio.h> 
#include <netdb.h> 
#include <curses.h>
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 

#define MAX_BUF 1024 
#define SA struct sockaddr 


int main( void )
{
	int menuchoice=0;
	
	while(menuchoice!='4')
	{
		clrscr();
		printf("\n\tChoose IED to display values from");
		printf("\n\t-----------------------------");
		printf("\n\t 1. NTNU - IED01 - Phase Shift");
		printf("\n\t 2. NTNU - IED02 - Voltage Conversion");
		printf("\n\t 4. Quit");
		printf("\n\n Enter IED number ");
		scanf(" %c", &menuchoice);
		
		// Currently, the EW is depentendt on that the HMI is configured to show
		// values from the IED chosen, so the program actually just dislplays 
		// the values that the HMI is currently operating on... 
		
		switch(menuchoice)
		{
			case '1':
					printf("Showing values from NTNU-IED01\n",1);
					socket_client("10.40.50.10", 6185);
					break;
			case '2':
					printf("Showing values from NTNU-IED02\n", 2);
					socket_client("10.40.50.11", 6185);
					break;
			case '3':
					printf("Quitting %c\n", 3);
					exit(0);
			default:
					printf("\nInvalid selection, quitting....\n");
					break;
		}
		//getch();
	}	
	return 0;
}

// Function to clear the screen
void clrscr()
{
	system("cls||clear");
}

//Function to handle connection between Client and Server
void socket_client(char srv_addr, int srv_port)
{
	int sockfd, connfd; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and varification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr(srv_addr); 
    servaddr.sin_port = htons(srv_port); 
  
    // connect the client socket to server socket 
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
        printf("connection with the server failed...\n"); 
        exit(0); 
    } 
    else
        printf("connected to the server..\n"); 
  
    // function for chat 
    ew_client(sockfd); 
  
    // close the socket 
    close(sockfd); 
}

//Function for chat between Client and Server
void ew_client(int sockfd)
{
	char buff[1024]; 
    int n; 
    for (;;) { 
        // clear the buffer
        bzero(buff, sizeof(buff)); 
        
        // read the buffer and display output
		read(sockfd, buff, sizeof(buff)); 
        printf("From Server : %s", buff); 
        if ((strncmp(buff, "exit", 4)) == 0) { 
            printf("Client Exit...\n"); 
            break; 
        } 
    } 
}