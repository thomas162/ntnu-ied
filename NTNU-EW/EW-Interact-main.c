#include <stdio.h> 
#include <netdb.h> 
#include <curses.h>
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 

#define MAX_BUF 1024 
#define SA struct sockaddr 
struct sockaddr_in server;

//Create a Socket for server communication
short SocketCreate(void)
{
    short hSocket;
    printf("Create the socket\n");
    hSocket = socket(AF_INET, SOCK_STREAM, 0);
    return hSocket;
}

//try to connect with server
int SocketConnect(int hSocket, char serverAddress, int port)
{
    int iRetval=-1;
    int ServerPort = port;
    struct sockaddr_in remote= {0};
    remote.sin_addr.s_addr = inet_addr(serverAddress); 
    remote.sin_family = AF_INET;
    remote.sin_port = htons(ServerPort);
    iRetval = connect(hSocket,(struct sockaddr *)&remote,sizeof(struct sockaddr_in));
    return iRetval;
}

// Send the data to the server and set the timeout of 20 seconds
int SocketSend(int hSocket,char* Rqst,short lenRqst)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 20;  /* 20 Secs Timeout */
    tv.tv_usec = 0;
    if(setsockopt(hSocket,SOL_SOCKET,SO_SNDTIMEO,(char *)&tv,sizeof(tv)) < 0)
    {
        printf("Time Out\n");
        return -1;
    }
    shortRetval = send(hSocket, Rqst, lenRqst, 0);
    return shortRetval;
}

// Send the data to the server and set the timeout of 20 seconds
int trySocketSend(int hSocket,char* Rqst,short lenRqst)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 20;  /* 20 Secs Timeout */
    tv.tv_usec = 0;
    if(setsockopt(hSocket,SOL_SOCKET,SO_SNDTIMEO,(char *)&tv,sizeof(tv)) < 0)
    {
        printf("Time Out\n");
        return -1;
    }
    shortRetval = send(hSocket, Rqst, lenRqst, 0);
    return shortRetval;
}

//receive the data from the server
int SocketReceive(int hSocket,char* Rsp,short RvcSize)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 20;  /* 20 Secs Timeout */
    tv.tv_usec = 0;
    if(setsockopt(hSocket, SOL_SOCKET, SO_RCVTIMEO,(char *)&tv,sizeof(tv)) < 0)
    {
        printf("Time Out\n");
        return -1;
    }
    shortRetval = recv(hSocket, Rsp, RvcSize, 0);
    printf("Response %s\n",Rsp);
    return shortRetval;
}


int main( void )
{
	int hSocket, read_size;
	
	char SendToServer = "Initial";
	char server_reply[200] = {0};

	int menuchoice=0;
	int menuchoice_ied1=0;
	while(menuchoice!='4')
	{
		clrscr();
		printf("\n\tChoose IED to operate on");
		printf("\n\t-----------------------------");
		printf("\n\t 1. NTNU - IED01 - Phase Shift");
		printf("\n\t 2. NTNU - IED02 - Voltage Conversion");
		printf("\n\t 3. Quit");
		printf("\n\n Enter IED number ");
		scanf(" %c", &menuchoice);
		switch(menuchoice)
		{
			case '1':					
					printf("Connecting to HMI - Operating on: NTNU - IED01\n",1);
					clrscr();
						printf("\n\tChoose command to send:");
						printf("\n\t-----------------------------");
						printf("\n\t 1. OLTC Tap - UP");
						printf("\n\t 2. OLTC Tap - DOWN");
						printf("\n\t 3. Quit");
						printf("\n\n Enter command number ");
						scanf(" %c", &menuchoice_ied1);
						switch(menuchoice_ied1)
						{
							case '1':
									// Send command TAP UP
								    
									//Create socket
									hSocket = SocketCreate();
									if(hSocket == -1)
									{
										printf("Could not create socket\n");
										return 1;
									}
									printf("Socket is created\n");
									//Connect to remote server
									if (SocketConnect(hSocket, "10.40.50.10", 6186) < 0)
									{
										perror("connect failed.\n");
										return 1;
									}
									printf("Sucessfully connected with server\n");
									
									SendToServer = "1";
									
									//Send data to the server
									trySocketSend(hSocket, SendToServer, strlen(SendToServer));
									//Received the data from the server
									read_size = SocketReceive(hSocket, server_reply, 200);
									printf("Server Response : %s\n\n",server_reply);
									close(hSocket);
									shutdown(hSocket,0);
									shutdown(hSocket,1);
									shutdown(hSocket,2);
									return 0;
									break;
							case '2':
									// Send command TAP DOWN
								    
									//Create socket
									hSocket = SocketCreate();
									if(hSocket == -1)
									{
										printf("Could not create socket\n");
										return 1;
									}
									printf("Socket is created\n");
									//Connect to remote server
									if (SocketConnect(hSocket) < 0)
									{
										perror("connect failed.\n");
										return 1;
									}
									printf("Sucessfully connected with server\n");
									
									SendToServer = "0";
									
									//printf("Enter the Message: ");
									//gets(SendToServer);
									//Send data to the server
									//trySocketSend(hSocket, "Start", strlen(SendToServer));
									trySocketSend(hSocket, SendToServer, strlen(SendToServer));
									//Received the data from the server
									read_size = SocketReceive(hSocket, server_reply, 200);
									printf("Server Response : %s\n\n",server_reply);
									close(hSocket);
									shutdown(hSocket,0);
									shutdown(hSocket,1);
									shutdown(hSocket,2);
									return 0;
									break;
							case '3':
									printf("Quitting %c\n", 4);
									exit(0);
							default:
									printf("\nInvalid selection, quitting....\n");
									break;
						}
					break;
			case '2':
					printf("Connecting to HMI - Operating on: NTNU-IED02\n",1);
					clrscr();
						printf("\n\tChoose command to send:");
						printf("\n\t-----------------------------");
						printf("\n\t 1. Conversion - ON");
						printf("\n\t 2. Conversion - OFF");
						printf("\n\t 3. Quit");
						printf("\n\n Enter command number ");
						scanf(" %c", &menuchoice_ied1);
						switch(menuchoice_ied1)
						{
							case '1':
									// Send command conversion on
								    
									//Create socket
									hSocket = SocketCreate();
									if(hSocket == -1)
									{
										printf("Could not create socket\n");
										return 1;
									}
									printf("Socket is created\n");
									//Connect to remote server
									if (SocketConnect(hSocket, "10.40.50.10", 6187) < 0)
									{
										perror("connect failed.\n");
										return 1;
									}
									printf("Sucessfully connected with server\n");
									
									SendToServer = "1";
									
									//Send data to the server
									trySocketSend(hSocket, SendToServer, strlen(SendToServer));
									//Received the data from the server
									read_size = SocketReceive(hSocket, server_reply, 200);
									printf("Server Response : %s\n\n",server_reply);
									close(hSocket);
									shutdown(hSocket,0);
									shutdown(hSocket,1);
									shutdown(hSocket,2);
									return 0;
									break;
							case '2':
									// Send command conversion off
								    
									//Create socket
									hSocket = SocketCreate();
									if(hSocket == -1)
									{
										printf("Could not create socket\n");
										return 1;
									}
									printf("Socket is created\n");
									//Connect to remote server
									if (SocketConnect(hSocket, "10.40.50.10", 6187) < 0)
									{
										perror("connect failed.\n");
										return 1;
									}
									printf("Sucessfully connected with server\n");
									
									SendToServer = "0";
									
									//Send data to the server
									trySocketSend(hSocket, SendToServer, strlen(SendToServer));
									//Received the data from the server
									read_size = SocketReceive(hSocket, server_reply, 200);
									printf("Server Response : %s\n\n",server_reply);
									close(hSocket);
									shutdown(hSocket,0);
									shutdown(hSocket,1);
									shutdown(hSocket,2);
									return 0;
									break;
							case '3':
									printf("Quitting %c\n", 4);
									exit(0);
							default:
									printf("\nInvalid selection, quitting....\n");
									break;
						}
			case '3':
					printf("Quitting %c\n", 4);
					exit(0);
			default:
					printf("\nInvalid selection, quitting....\n");
					break;
		}
	}	
	return 0;
}